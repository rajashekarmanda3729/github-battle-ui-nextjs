"use client"

import { useSelector } from "react-redux"
import GithubRepos from "./components/GithubRepos"
import Header from "./components/Header"
import Languages from "./components/Languages"

const Home = () => {

  const { darkMode } = useSelector(store => store.github)

  return (
    <div className={`container-fluid ${darkMode && "bg-dark bg-opacity-75"}`}>
      <Header />
      <Languages />
      <GithubRepos />
    </div>
  )
}
export default Home