"use client"

const Loading = () => {
    return (
        <div className='row'>
            <div className="col-12 d-flex justify-content-center align-items-center vh-100">
                <div className="spinner-border text-warning" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>
    )
}

export default Loading