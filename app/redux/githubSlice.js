"use client"

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
    language: 'All',
    darkMode: true,
    reposData: [],
    isLoading: false,
    searchWord: ''
}

export const getRepos = createAsyncThunk('github/getRepos',
    async (name = 'All', asyncThunk) => {
        const response = await fetch(`https://api.github.com/search/repositories?q=stars:%3E1+language:${name}&sort=stars&order=desc&type=Repositories`)
        const data = response.json()
        return data
    })

export const githubSlice = createSlice({
    name: 'github',
    initialState,
    reducers: {
        onChangeLanguage: (state, { payload }) => {
            state.language = payload
        },
        onToggleDarkMode: (state) => {
            state.darkMode = !state.darkMode

        },
        onSearchRepo: (state, { payload }) => {
            state.searchWord = payload
        }
    },
    extraReducers: {
        [getRepos.pending]: (state) => {
            state.isLoading = true
        },
        [getRepos.fulfilled]: (state, { payload }) => {
            state.reposData = payload.items
            state.isLoading = false
        },
        [getRepos.rejected]: (state) => {
            state.isLoading = false
        }
    }
})

export const { onChangeLanguage, onToggleDarkMode, onSearchRepo } = githubSlice.actions

export default githubSlice.reducer