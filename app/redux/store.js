"use client"
import { configureStore } from "@reduxjs/toolkit";
import githubSlice from "./githubSlice";

export const store = configureStore({
    reducer:{
        github:githubSlice
    }
})