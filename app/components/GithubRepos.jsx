"use client"

import { useDispatch, useSelector } from "react-redux"
import Repo from "./Repo"
import { useEffect } from "react"
import { getRepos } from "../redux/githubSlice"
import Loading from "../Loading"


const GithubRepos = () => {

    const { reposData, isLoading, searchWord } = useSelector(store => store.github)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getRepos())
    }, [])

    const filterRepos = reposData.filter(repo => {
        if (repo.owner.login.toLowerCase().includes(searchWord.toLowerCase())) {
            return repo
        }
    })

    return (
        <div className="row">
            <div className="col-12 d-flex flex-wrap justify-content-center">
                {
                    (filterRepos.length > 0 && !isLoading) ?
                        filterRepos.map((repo, index) => {
                            return <Repo itemDetails={repo} index={index} key={index} />
                        }) :
                        <Loading />
                }
            </div>

        </div>
    )
}

export default GithubRepos