"use client"
import { useSelector } from 'react-redux'
import { CgGitFork } from 'react-icons/cg'
import { BsFillExclamationTriangleFill, BsFillPersonFill } from 'react-icons/bs'
import { AiFillStar } from 'react-icons/ai'

const Repo = ({ itemDetails, index }) => {
    const { avatar_url, login } = itemDetails.owner
    const { stargazers_count, forks_count, open_issues_count, html_url } = itemDetails
    const { darkMode } = useSelector(store => store.github)


    return (
        <div className={`card m-3 p-1 shadow pt-4`} style={{ width: "24rem", backgroundColor: `${darkMode ? 'black' : 'lightgray'}` }}>
            <div className="d-flex flex-column justify-content-center align-items-center">
                <h1 className={`${darkMode ? 'text-light' : 'text-dark'}`}>#{index + 1}</h1>
                <img src={avatar_url} className="card-img-top w-50" alt={login} />
            </div>
            <div className="card-body text-center">
                <a href={html_url} className='text-decoration-none'>
                    <h3 className="card-title text-danger">{login}</h3>
                </a>
            </div>
            <div className="d-flex flex-column ms-3">
                <a href={`https://github.com/${login}`} className={`text-decoration-none ${darkMode ? 'text-light' : 'text-dark'}`}>
                    <h4 className="text-start mb-2"><BsFillPersonFill color='orange' className='m-2 mt-1' size={'1.6em'} />{login}</h4>
                </a>
                <h5 className={`text-start mb-2 ${darkMode && 'text-light'}`}>
                    <AiFillStar color='yellow' className='m-2 mt-1' size={'1.7em'} />
                    {stargazers_count} stars
                </h5>
                <h5 className={`text-start mb-2 ${darkMode && 'text-light'}`}><CgGitFork color='blue' className='m-2 mt-1' size={'1.7em'} />
                    {forks_count} forks
                </h5>
                <h5 className={`text-start mb-3 ${darkMode && 'text-light'}`}><BsFillExclamationTriangleFill color='brown' size={'1.3em'}
                    className='m-2 mt-1' />
                    {open_issues_count} issues
                </h5>
            </div>
        </div>

    )
}

export default Repo