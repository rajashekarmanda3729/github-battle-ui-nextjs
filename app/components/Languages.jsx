"use client"

import { useDispatch, useSelector } from "react-redux"
import { languagesArr } from "../redux/data"
import { getRepos, onChangeLanguage } from "../redux/githubSlice"

const Languages = () => {

    const { language, darkMode } = useSelector(store => store.github)
    const dispatch = useDispatch()

    return (
        <div className="row">
            <div className="col-12 d-flex justify-content-center mt-1">
                {
                    languagesArr.map(eachLanguage => {
                        return <button type="button"
                            onClick={() => {
                                dispatch(onChangeLanguage(eachLanguage))
                                dispatch(getRepos(eachLanguage))
                            }}
                            className={`btn border-0 ${darkMode ? eachLanguage != language ? 'text-light' : 'text-danger' : eachLanguage != language ? 'text-dark' : 'text-danger'}`}
                            value={eachLanguage}
                            key={eachLanguage}><h2>{eachLanguage}</h2></button>
                    })
                }
            </div>
        </div >
    )
}

export default Languages