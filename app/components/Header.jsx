'use client'

import { useDispatch, useSelector } from "react-redux"
import { onSearchRepo, onToggleDarkMode } from "../redux/githubSlice"
import { FcSearch } from 'react-icons/fc'

const Header = () => {

    const { darkMode, searchWord } = useSelector(store => store.github)
    const dispatch = useDispatch()

    const handleChange = () => {
        dispatch(onToggleDarkMode())
    }

    return (
        <div className="row">
            <div className={`col-12 d-flex p-3 bg-dark bg-opacity-${darkMode ? '100' : '25'} justify-content-between`}>
                <div className="col-2 d-flex justify-content-between">
                    <h2 className="text-danger">popular</h2>
                    <h2 className="text-light">Battle</h2>
                </div>
                <div className="col-6 d-flex bg-light rounded-4 border-0">
                    <input type="text"
                        className="w-100 h-100 border-0 bg-transparent rounded-4 fs-3 ps-3 outline-stylen-none"
                        placeholder="Search"
                        value={searchWord}
                        onChange={(e) => dispatch(onSearchRepo(e.target.value))}
                    />
                    {searchWord == '' && <FcSearch size={40} className="me-2 mt-1" />}
                </div>
                <button className="d-flex col-2 text-light justify-content-end bg-transparent border-0"
                    onClick={handleChange}>
                    <h2>{darkMode ? '🔦 Dark ' : '💡 Light '}Mode</h2>
                </button>
            </div>
        </div>
    )
}

export default Header